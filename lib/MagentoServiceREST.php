<?php

namespace Kowal\IntegracjaArtpol\lib;

use Exception;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class MagentoServiceREST
 *
 * Wzięte z Wm21w\MetadataImporter
 */

// Funckje do obsługi REST API Magento
// Uzywane tylko w klasie MagentoUtils
class MagentoServiceREST
{
    private $accessToken = "brak_tokenu";
    private $webApiUrl = "https://www.domena.xx/index.php/rest";
    private $username = "";
    private $password = "";

    public function __construct(
        ScopeConfigInterface $scopeConfig
    )
    {
        $this->_scopeConfig = $scopeConfig;
        $username = $this->_scopeConfig->getValue('artpol/api/username', ScopeInterface::SCOPE_STORE);
        $password = $this->_scopeConfig->getValue('artpol/api/password', ScopeInterface::SCOPE_STORE);
        $url = $this->_scopeConfig->getValue('artpol/api/url', ScopeInterface::SCOPE_STORE);
        $this->setAccessFields($url."index.php/rest", $username, $password);
    }

    public function setAccessFields($webApiUrl, $username, $password)
    {
        $this->webApiUrl = $webApiUrl;
        $this->username = $username;
        $this->password = $password;
    }

    public function checkAccessFields()
    {
        if (($this->webApiUrl === null || $this->username === null || $this->password === null)) {
            throw new Exception("MagentoServiceREST: missing username/password/webapiurl, use setAccessFields()");
        }
    }

    public function deleteProduct($sku)
    {
        $httpCode = null;
        $response = $this->mageWebApiRequest("/V1/products/$sku", null, "DELETE", $httpCode);
    }

    //dokumentacja zayptań: https://devdocs.magento.com/swagger/index_22.html#/


    /**
     * @param array $product
     * @return object Nowy produkt jako obiekt PHP
     */
    public function addProduct($product)
    {
        // zobacz https://devdocs.magento.com/swagger/index_22.html#!/catalogProductRepositoryV1

        $customAttributes = [];
        $notCustomAttributes = ["sku", "name", "price", "qty", "_cat_id", "attribute_set_id", "type_id", "visibility", "_store_view_code", "media_gallery_entries", "status", "tier_prices", "options", "product_links", "weight", "id", "configurable_product_links", "configurable_product_options"];
        foreach ($product as $attrName => $attrVal) {
            if (!in_array($attrName, $notCustomAttributes)) {
                $customAttributes[] = [
                    'attribute_code' => $attrName,
                    'value' => $attrVal
                ];
            }
        }
        if (isset($product["_cat_id"]) && $product["_cat_id"]) {
            $customAttributes[] = [
                'attribute_code' => 'category_ids',
                'value' => [(string)$product["_cat_id"]]
            ];
        }

        $productMagentoData = [
            "product" => [
                "custom_attributes" => $customAttributes,
            ],
        ];

        $extAttrs = [];

        $extensionAttributes = ["stockItem", "configurable_product_options", "configurable_product_links"];
        foreach ($extensionAttributes as $param) {
            if (isset($product[$param])) {
                $extAttrs[$param] = $product[$param];
            }
        }

        $basicParams = ["attribute_set_id", "visibility", "sku", "type_id", "name", 'price', 'media_gallery_entries', 'status', 'options', "product_links", "weight", "id"];
        foreach ($basicParams as $param) {
            if (isset($product[$param])) {
                $productMagentoData["product"][$param] = ($product[$param]);
            }
        }
        if (isset($product["qty"])) {
            $extAttrs["stockItem"] = [
                "qty" => $product["qty"],
                "is_in_stock" => ($product["qty"] > 0) ? (true) : (false),
                "stock_status_changed_auto" => 1
            ];
        }
        $productMagentoData["product"]["extension_attributes"] = $extAttrs;
        $prefix = "";
        if (isset($product["_store_view_code"])) {
            $prefix = "/" . $product["_store_view_code"];
        } else {
            $prefix = '/all';
        }

//        print_r($productMagentoData);
        $httpCode = null;
        $resource = "$prefix/V1/products";
        $response = $this->mageWebApiRequest($resource, $productMagentoData, "POST", $httpCode);
//        var_dump($response);
        if ($httpCode == 404) {
//             die();
            $msg = "POST " . $resource . " : " . $response->message . (isset($response->parameters) ? print_r($response->parameters, true) : "") . (isset($response->errors) ? print_r($response->errors, true) : "");
//            $msg .=  . "\n data=" . print_r($productMagentoData, true);
            throw new Exception($msg);
        } else if ($httpCode == 400) {
//            $msg = "POST " . $resource ." : ". $response->message;
            $msg = $response->message . (isset($response->parameters) ? print_r($response->parameters, true) : "") . (isset($response->errors) ? print_r($response->errors, true) : "");
//            $msg .=  print_r($response, true);
//            throw new \Exception($msg);
            return $msg;
        }
        return $response;
    }

    public function mageGetProduct($sku)
    {
        $data = [];
        $httpCode = null;
        $sku = urlencode($sku);
        $response = $this->mageWebApiRequest("/V1/products/$sku", $data, "GET", $httpCode);
        if ($httpCode == 404) {
            return null;
        }
        return $response;
    }

    // jak label pusty zwraca CHYBA ""
    public function getOptionIdInDropDownAttribute($attrCode, $label)
    {
        $options = $this->mageWebApiRequest(
            "/V1/products/attributes/$attrCode/options", null, "GET"
        );
        $returnOptionId = null;
        foreach ($options as $option) {
            if ($option->label == $label) {
                $returnOptionId = $option->value;
                break;
            }
        }
        return $returnOptionId;
    }

    public function addOptionToAttribute($attrCode, $label, $value = null)
    {
        // rest api z https://magento.stackexchange.com/questions/165047/magento-2-rest-api-how-do-i-add-values-to-dropdown-product-attribute
        $data = [
            "option" => [
                "label" => $label,
            ]
        ];
        if ($value) $data["option"]["value"] = $value;
        $response = $this->mageWebApiRequest(
            "/V1/products/attributes/$attrCode/options", $data, "POST"
        );
        return $response;
    }

    /**
     * @param array $query Tablica ["name" => "string", "value" => "string"]
     * @return int Id kategorii w Magento lub -1
     */
    public function mageFindCatByAttr($query)
    {
        $data = [
            "searchCriteria[filterGroups][0][filters][0][field]" => $query["name"],
            "searchCriteria[filterGroups][0][filters][0][value]" => $query["value"],
            "searchCriteria[filterGroups][0][filters][0][conditionType]" => "eq",
            "searchCriteria[pageSize]" => 1,
            "searchCriteria[currentPage]" => 1,
        ];
        $response = $this->mageWebApiRequest("/V1/categories/list", $data, "GET");
        if (isset($response->items)) {
            $foundItems = $response->items;
            if (count($foundItems) == 0) {
                // nie znaleziono
                return -1;
            }
            return $foundItems[0]->id;
        } else {
            return null;
        }

    }

    public function mageAddCategory($category)
    {
        $customAttributes = [];
        $notCustomAttributes = ["name", "parent_id", "id", "include_in_menu", "is_active"];
        foreach ($category as $attrName => $attrVal) {
            if (!in_array($attrName, $notCustomAttributes)) {
                $customAttributes[] = [
                    'attribute_code' => $attrName,
                    'value' => $attrVal
                ];
            }
        }

        $subGroupMageCatData = [
            "category" => [
//                "name" => $category["name"],
//                "is_active" => true,
//                "parent_id" => $category["parent_id"],
                "custom_attributes" => $customAttributes,
            ]
        ];
        if (isset($category["id"]) && $category["id"]) {
            $subGroupMageCatData["category"]["id"] = $category["id"];
        }
        if (isset($category["name"]) && $category["name"]) {
            $subGroupMageCatData["category"]["name"] = $category["name"];
        }
        if (isset($category["parent_id"]) && $category["parent_id"]) {
            $subGroupMageCatData["category"]["parent_id"] = $category["parent_id"];
        }
        if (isset($category["is_active"]) && $category["is_active"]) {
            $subGroupMageCatData["category"]["is_active"] = $category["is_active"];
        }

        $httpCode = null;

        $response = $this->mageWebApiRequest("/all/V1/categories", $subGroupMageCatData, "POST", $httpCode);
//        var_dump($response);
        if (isset($response->id)) {
            return $response->id;
        } else {
            throw new Exception("magentorest mageAddCategory: $httpCode ." . print_r($response, true) . 'request' . print_r($category, true));
        }
    }

    public function requestMageToken()
    {
        $this->checkAccessFields();

        $data = [
            "username" => $this->username,
            "password" => $this->password
        ];
        $response = $this->mageWebApiRequest("/V1/integration/admin/token", $data, "POST");
        if (!is_string($response)) {
            throw new Exception("requestMageToken failed:\n $response");
        }
        $this->accessToken = $response;
        return $response;
    }

    public function mageWebApiRequest($resource, $data, $requestType, &$httpCodeParam = NULL)
    {
        $this->checkAccessFields();

        $url = "";
        $ch = curl_init();
        $jsonData = "";

        $curlOptions = [
            CURLOPT_CUSTOMREQUEST => $requestType,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array("Content-Type: application/json", "Authorization: Bearer " . $this->accessToken)
        ];
        if ($requestType == "GET") {
            if ($data) {
                $query = http_build_query($data);
                $url = $this->webApiUrl . $resource . "?" . $query;
            } else {
                $url = $this->webApiUrl . $resource;
            }
        } else {
            if ($data) {
                $jsonData = json_encode($data);
                if ($jsonData === false) {
                    throw new Exception("Could not encode JSON code:" . json_last_error());
                }
                $curlOptions[CURLOPT_POSTFIELDS] = $jsonData;
            }
            $url = $this->webApiUrl . $resource;
        }
        $curlOptions[CURLOPT_URL] = $url;
        curl_setopt_array($ch, $curlOptions);

        $response = curl_exec($ch);
        $contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (curl_error($ch)) {
            $errno = curl_errno($ch);
            $error = curl_error($ch);
            throw new Exception("$httpcode $requestType $url : curl error $errno: $error");
        }

        if (strpos($contentType, "application/json") > -1) {
            $response = json_decode($response);
        } else {
            throw new Exception("Expected json: $requestType $url response:\n $response");
        }
        curl_close($ch);

        $httpCodeParam = $httpcode;
        switch ($httpcode) {
            case 200: /* OK */
                return $response;
                break;
            case 401: // Unauthorized
                $this->requestMageToken();
                return $this->mageWebApiRequest($resource, $data, $requestType, $httpCodeParam);
                break;
            case 404: /* Not found */
                return $response;
                break;
            case 400: /* Bad request */
                return $response;
                break;
            default:
                $msg = "$httpcode - $requestType $url data:\n" . print_r($data, true) . "\n response:\n" . print_r($response, true);
                throw new Exception($msg);
                break;
        }

    }


}

?>