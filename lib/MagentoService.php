<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 2019-06-13
 * Time: 21:50
 */

namespace Kowal\IntegracjaArtpol\lib;


use Magento\Framework\App\ResourceConnection;

class MagentoService
{
    public $store_id = 0;

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    public function __construct(
        ResourceConnection $resourceConnection
    )
    {
        $this->resourceConnection = $resourceConnection;
    }


    /**
     * @return mixed
     */
    private function _getReadConnection()
    {
        return $this->_getConnection('core_read');
    }


    /**
     * @param string $type
     * @return mixed
     */
    private function _getConnection($type = 'core_read')
    {
        return $this->resourceConnection->getConnection($type);
    }

    /**
     * @param $tableName
     * @return mixed
     */
    private function _getTableName($tableName)
    {
        return $this->resourceConnection->getTableName($tableName);
    }

    /**
     * @param $sku
     * @return mixed
     */
    public function _getNiepowiazaneProdukty()
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT id FROM " . $this->_getTableName('kowal_integracjaartpol_artpol') . " WHERE powiazany = ?";
        return $connection->fetchAssoc(
            $sql,
            [
                0
            ]
        );
    }

    public function _getNiepowiazaneProduktyByAttr($attr = "")
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT entity_id AS id, sku AS sku FROM " . $this->_getTableName('catalog_product_entity') . " WHERE sku LIKE \"%".$attr."%\"";
        return $connection->fetchAssoc(
            $sql
        );
    }

    // select * from `table` where `yourfield` >= DATE_SUB(CURDATE(), INTERVAL 3 MONTH)
    public function _getProduktyDoUsuniecia($months)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT symbol FROM " . $this->_getTableName('kowal_integracjaartpol_artpol') . " WHERE last_update < DATE_SUB(CURDATE(), INTERVAL ? MONTH)";
        return $connection->fetchAssoc(
            $sql,
            [
                $months
            ]
        );
    }

    public function _getProduktyDoUsunieciaNiepowiazane()
    {
        $connection = $this->_getConnection('core_read');
        $sql = "select a.sku as sku from " . $this->_getTableName('catalog_product_entity') . " as a left join " . $this->_getTableName('kowal_integracjaartpol_artpol') . " as b on a.sku = concat(\"artpol_\",b.symbol) where b.symbol is null AND a.sku LIKE \"%artpol_%\"";
        return $connection->fetchAssoc(
            $sql
        );
    }

    //DELETE FROM `kowal_integracjaartpol_artpol` WHERE `kowal_integracjaartpol_artpol`.`artpol_id` = 4671
    public function _deleteProductInfo($symbol)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "DELETE FROM " . $this->_getTableName('kowal_integracjaartpol_artpol') . " WHERE symbol = ?";
        return $connection->query(
            $sql,
            [
                $symbol
            ]
        );
    }

    public function _getIdFromSku($sku)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT entity_id FROM " . $this->_getTableName('catalog_product_entity') . " WHERE sku = ?";
        return $connection->fetchOne(
            $sql,
            [
                $sku
            ]
        );
    }

    public function checkIfSkuExists($sku, $producent = '')
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT sku, type_id FROM " . $this->_getTableName('catalog_product_entity') . " WHERE sku IN (?,?)";
        return $connection->fetchRow($sql, [trim($sku), trim($producent) . '_' . trim($sku)]);
    }
}