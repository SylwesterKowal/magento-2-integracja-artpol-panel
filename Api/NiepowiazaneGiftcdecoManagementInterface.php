<?php
/**
 * A Magento 2 module named Kowal/IntegracjaArtpol
 * Copyright (C) 2019
 *
 * This file included in Kowal/IntegracjaArtpol is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */

namespace Kowal\IntegracjaArtpol\Api;

interface NiepowiazaneGiftcdecoManagementInterface
{

    /**
     * @return mixed
     */
    public function getNiepowiazaneGiftcdeco();
}