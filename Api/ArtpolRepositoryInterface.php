<?php


namespace Kowal\IntegracjaArtpol\Api;

use Kowal\IntegracjaArtpol\Api\Data\ArtpolInterface;
use Kowal\IntegracjaArtpol\Api\Data\ArtpolSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

interface ArtpolRepositoryInterface
{

    /**
     * Save Artpol
     * @param ArtpolInterface $artpol
     * @return ArtpolInterface
     * @throws LocalizedException
     */
    public function save(
        ArtpolInterface $artpol
    );

    /**
     * Retrieve Artpol
     * @param string $artpolId
     * @return ArtpolInterface
     * @throws LocalizedException
     */
    public function getById($artpolId);

    /**
     * Retrieve Artpol matching the specified criteria.
     * @param SearchCriteriaInterface $searchCriteria
     * @return ArtpolSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(
        SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Artpol
     * @param ArtpolInterface $artpol
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(
        ArtpolInterface $artpol
    );

    /**
     * Delete Artpol by ID
     * @param string $artpolId
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($artpolId);
}
