<?php


namespace Kowal\IntegracjaArtpol\Api;

interface NiepowiazaneManagementInterface
{

    /**
     * @return mixed
     */
    public function getNiepowiazane();

}