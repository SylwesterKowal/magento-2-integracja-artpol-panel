<?php


namespace Kowal\IntegracjaArtpol\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface ArtpolInterface extends ExtensibleDataInterface
{

    const PRICE = 'price';
    const URL = 'url';
    const WEIGHT = 'weight';
    const POWIAZANY = 'powiazany';
    const MAGAZYNY = 'magazyny';
    const NAME = 'name';
    const STOCKS = 'stocks';
    const ARTPOL_ID = 'artpol_id';
    const LAST_UPDATE = 'last_update';
    const SYMBOL = 'symbol';
    const CAT = 'cat';
    const ID = 'id';

    /**
     * Get artpol_id
     * @return string|null
     */
    public function getArtpolId();

    /**
     * Set artpol_id
     * @param string $artpolId
     * @return ArtpolInterface
     */
    public function setArtpolId($artpolId);

    /**
     * Get id
     * @return string|null
     */
    public function getId();

    /**
     * Set id
     * @param string $id
     * @return ArtpolInterface
     */
    public function setId($id);

//    /**
//     * Retrieve existing extension attributes object or create a new one.
//     * @return ArtpolExtensionInterface|null
//     */
//    public function getExtensionAttributes();
//
//    /**
//     * Set an extension attributes object.
//     * @param ArtpolExtensionInterface $extensionAttributes
//     * @return $this
//     */
//    public function setExtensionAttributes(
//        ArtpolExtensionInterface $extensionAttributes
//    );

    /**
     * Get name
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     * @param string $name
     * @return ArtpolInterface
     */
    public function setName($name);

    /**
     * Get price
     * @return string|null
     */
    public function getPrice();

    /**
     * Set price
     * @param string $price
     * @return ArtpolInterface
     */
    public function setPrice($price);

    /**
     * Get symbol
     * @return string|null
     */
    public function getSymbol();

    /**
     * Set symbol
     * @param string $symbol
     * @return ArtpolInterface
     */
    public function setSymbol($symbol);

    /**
     * Get cat
     * @return string|null
     */
    public function getCat();

    /**
     * Set cat
     * @param string $cat
     * @return ArtpolInterface
     */
    public function setCat($cat);

    /**
     * Get url
     * @return string|null
     */
    public function getUrl();

    /**
     * Set url
     * @param string $url
     * @return ArtpolInterface
     */
    public function setUrl($url);

    /**
     * Get magazyny
     * @return string|null
     */
    public function getMagazyny();

    /**
     * Set magazyny
     * @param string $magazyny
     * @return ArtpolInterface
     */
    public function setMagazyny($magazyny);

    /**
     * Get weight
     * @return string|null
     */
    public function getWeight();

    /**
     * Set weight
     * @param string $weight
     * @return ArtpolInterface
     */
    public function setWeight($weight);

    /**
     * Get stocks
     * @return string|null
     */
    public function getStocks();

    /**
     * Set stocks
     * @param string $stocks
     * @return ArtpolInterface
     */
    public function setStocks($stocks);

    /**
     * Get powiazany
     * @return string|null
     */
    public function getPowiazany();

    /**
     * Set powiazany
     * @param string $powiazany
     * @return ArtpolInterface
     */
    public function setPowiazany($powiazany);

    /**
     * Get last_update
     * @return string|null
     */
    public function getLastUpdate();

    /**
     * Set last_update
     * @param string $lastUpdate
     * @return ArtpolInterface
     */
    public function setLastUpdate($lastUpdate);
}
