<?php


namespace Kowal\IntegracjaArtpol\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface MagazynySearchResultsInterface extends SearchResultsInterface
{

    /**
     * Get Magazyny list.
     * @return MagazynyInterface[]
     */
    public function getItems();

    /**
     * Set id list.
     * @param MagazynyInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
