<?php


namespace Kowal\IntegracjaArtpol\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface MagazynyInterface extends ExtensibleDataInterface
{

    const MAGAZYNY_ID = 'magazyny_id';
    const NAZWA = 'Nazwa';
    const ID = 'id';

    /**
     * Get magazyny_id
     * @return string|null
     */
    public function getMagazynyId();

    /**
     * Set magazyny_id
     * @param string $magazynyId
     * @return MagazynyInterface
     */
    public function setMagazynyId($magazynyId);

    /**
     * Get id
     * @return string|null
     */
    public function getId();

    /**
     * Set id
     * @param string $id
     * @return MagazynyInterface
     */
    public function setId($id);

//    /**
//     * Retrieve existing extension attributes object or create a new one.
//     * @return MagazynyExtensionInterface|null
//     */
//    public function getExtensionAttributes();
//
//    /**
//     * Set an extension attributes object.
//     * @param MagazynyExtensionInterface $extensionAttributes
//     * @return $this
//     */
//    public function setExtensionAttributes(
//        MagazynyExtensionInterface $extensionAttributes
//    );

    /**
     * Get Nazwa
     * @return string|null
     */
    public function getNazwa();

    /**
     * Set Nazwa
     * @param string $nazwa
     * @return MagazynyInterface
     */
    public function setNazwa($nazwa);
}
