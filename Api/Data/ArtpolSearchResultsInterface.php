<?php


namespace Kowal\IntegracjaArtpol\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface ArtpolSearchResultsInterface extends SearchResultsInterface
{

    /**
     * Get Artpol list.
     * @return ArtpolInterface[]
     */
    public function getItems();

    /**
     * Set id list.
     * @param ArtpolInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
