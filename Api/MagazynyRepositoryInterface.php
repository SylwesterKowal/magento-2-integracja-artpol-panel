<?php


namespace Kowal\IntegracjaArtpol\Api;

use Kowal\IntegracjaArtpol\Api\Data\MagazynyInterface;
use Kowal\IntegracjaArtpol\Api\Data\MagazynySearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

interface MagazynyRepositoryInterface
{

    /**
     * Save Magazyny
     * @param MagazynyInterface $magazyny
     * @return MagazynyInterface
     * @throws LocalizedException
     */
    public function save(
        MagazynyInterface $magazyny
    );

    /**
     * Retrieve Magazyny
     * @param string $magazynyId
     * @return MagazynyInterface
     * @throws LocalizedException
     */
    public function getById($magazynyId);

    /**
     * Retrieve Magazyny matching the specified criteria.
     * @param SearchCriteriaInterface $searchCriteria
     * @return MagazynySearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(
        SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Magazyny
     * @param MagazynyInterface $magazyny
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(
        MagazynyInterface $magazyny
    );

    /**
     * Delete Magazyny by ID
     * @param string $magazynyId
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($magazynyId);
}
