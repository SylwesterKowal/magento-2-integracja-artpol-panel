<?php


namespace Kowal\IntegracjaArtpol\Console\Command;

use Kowal\IntegracjaArtpol\lib\MagentoService;
use Kowal\IntegracjaArtpol\lib\MagentoServiceREST;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Gallery\Processor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteArtpolProduct extends Command
{

    const NAME_ARGUMENT = "name";
    const NAME_OPTION = "option";


    /**
     * DeleteArtpolProduct constructor.
     * @param MagentoServiceREST $magentoServiceREST
     * @param MagentoService $magentoService
     */
    public function __construct(
        MagentoServiceREST $magentoServiceREST,
        MagentoService $magentoService,
        Product $productModel,
        Processor $imageProcessorss
    )
    {
        parent::__construct();
        $this->magentoServiceREST = $magentoServiceREST;
        $this->magentoService = $magentoService;
        $this->productModel = $productModel;
        $this->imageProcessor = $imageProcessorss;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    )
    {
        $sku = $input->getArgument(self::NAME_ARGUMENT);
        $produkcja = $input->getOption(self::NAME_OPTION);
        if (!$produkcja) {
            $output->writeln("TEST DELETE SKU:" . $sku);
            if ($this->magentoService->checkIfSkuExists($sku)) {
                $this->deleteImages($sku);
                $this->magentoServiceREST->deleteProduct($sku);
            } else {
                $output->writeln("Produkt o SKU: {$sku} nie istnieje");
            }
            $output->writeln("END TEST");
        } else {
            if ($products = $this->magentoService->_getProduktyDoUsuniecia(2)) {
                $count = count($products);
                $output->writeln("PRODUKCJA Ilość produktów: {$count}");
                $i = 1;
                foreach ($products as $p) {
                    $sku = 'artpol_' . $p['symbol'];
                    if ($this->magentoService->checkIfSkuExists($sku)) {
                        $this->deleteImages($sku);
                        $this->magentoServiceREST->deleteProduct($sku);
                        $this->magentoService->_deleteProductInfo($p['symbol']);
                        $output->writeln("{$i}. USUNIĘTO SKU: {$sku}");
                    } else {
                        $output->writeln("Produkt o SKU: {$sku} nie istnieje");
                        $this->magentoService->_deleteProductInfo($p['symbol']);
                    }
                    $i++;
                }
            }
            


        }
    }

    private function deleteImages($sku)
    {
        $product = $this->productModel->load($this->magentoService->_getIdFromSku($sku));
        if ($gallery = $product->getMediaGalleryImages()) {
            foreach ($gallery as $image) {
                $this->imageProcessor->removeImage($product, $image->getFile());
            }
        }
        $product->save();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_integracjaartpol:deleteartpolproduct");
        $this->setDescription("Usuwanie wycofanych produktow");
        $this->setDefinition([
            new InputArgument(self::NAME_ARGUMENT, InputArgument::OPTIONAL, "SKU"),
            new InputOption(self::NAME_OPTION, "-p", InputOption::VALUE_NONE, "Tryb produkcyjny")
        ]);
        parent::configure();
    }
}