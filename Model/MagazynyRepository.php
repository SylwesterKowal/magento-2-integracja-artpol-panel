<?php


namespace Kowal\IntegracjaArtpol\Model;

use Exception;
use Kowal\IntegracjaArtpol\Api\Data\MagazynyInterface;
use Kowal\IntegracjaArtpol\Model\ResourceModel\Magazyny\CollectionFactory as MagazynyCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Store\Model\StoreManagerInterface;
use Kowal\IntegracjaArtpol\Api\MagazynyRepositoryInterface;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\DataObjectHelper;
use Kowal\IntegracjaArtpol\Model\ResourceModel\Magazyny as ResourceMagazyny;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Exception\NoSuchEntityException;
use Kowal\IntegracjaArtpol\Api\Data\MagazynyInterfaceFactory;
use Kowal\IntegracjaArtpol\Api\Data\MagazynySearchResultsInterfaceFactory;

class MagazynyRepository implements MagazynyRepositoryInterface
{

    protected $dataMagazynyFactory;

    private $collectionProcessor;

    protected $extensibleDataObjectConverter;
    protected $magazynyCollectionFactory;

    protected $dataObjectHelper;

    protected $extensionAttributesJoinProcessor;

    private $storeManager;

    protected $magazynyFactory;

    protected $resource;

    protected $searchResultsFactory;

    protected $dataObjectProcessor;


    /**
     * @param ResourceMagazyny $resource
     * @param MagazynyFactory $magazynyFactory
     * @param MagazynyInterfaceFactory $dataMagazynyFactory
     * @param MagazynyCollectionFactory $magazynyCollectionFactory
     * @param MagazynySearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceMagazyny $resource,
        MagazynyFactory $magazynyFactory,
        MagazynyInterfaceFactory $dataMagazynyFactory,
        MagazynyCollectionFactory $magazynyCollectionFactory,
        MagazynySearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->magazynyFactory = $magazynyFactory;
        $this->magazynyCollectionFactory = $magazynyCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataMagazynyFactory = $dataMagazynyFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        MagazynyInterface $magazyny
    ) {
        /* if (empty($magazyny->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $magazyny->setStoreId($storeId);
        } */
        
        $magazynyData = $this->extensibleDataObjectConverter->toNestedArray(
            $magazyny,
            [],
            MagazynyInterface::class
        );
        
        $magazynyModel = $this->magazynyFactory->create()->setData($magazynyData);
        
        try {
            $this->resource->save($magazynyModel);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the magazyny: %1',
                $exception->getMessage()
            ));
        }
        return $magazynyModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($magazynyId)
    {
        $magazyny = $this->magazynyFactory->create();
        $this->resource->load($magazyny, $magazynyId);
        if (!$magazyny->getId()) {
            throw new NoSuchEntityException(__('Magazyny with id "%1" does not exist.', $magazynyId));
        }
        return $magazyny->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        SearchCriteriaInterface $criteria
    ) {
        $collection = $this->magazynyCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            MagazynyInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        MagazynyInterface $magazyny
    ) {
        try {
            $magazynyModel = $this->magazynyFactory->create();
            $this->resource->load($magazynyModel, $magazyny->getMagazynyId());
            $this->resource->delete($magazynyModel);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Magazyny: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($magazynyId)
    {
        return $this->delete($this->getById($magazynyId));
    }
}
