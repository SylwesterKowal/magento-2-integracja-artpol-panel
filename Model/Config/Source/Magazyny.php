<?php


namespace Kowal\IntegracjaArtpol\Model\Config\Source;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Option\ArrayInterface;

class Magazyny implements ArrayInterface
{
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    public function __construct(
        ResourceConnection $resourceConnection
    )
    {
        $this->resourceConnection = $resourceConnection;
    }

    public function toOptionArray()
    {
        return $this->getOptionArray();
//        return [['value' => 'Magazyn Giełda Wrocław [2]', 'label' => __('Magazyn Giełda Wrocław [2]')], ['value' => 'Magazyn G2 [4]', 'label' => __('Magazyn G2 [4]')], ['value' => 'Magazyn Łódź [9]', 'label' => __('Magazyn Łódź [9]')], ['value' => 'Magazyn Lublin [11]', 'label' => __('Magazyn Lublin [11]')], ['value' => 'Magazyn Koszalin [25]', 'label' => __('Magazyn Koszalin [25]')]];
    }

    public function toArray()
    {
        return $this->getArray();
    }


    private function getOptionArray()
    {
        $connection = $this->_getReadConnection();
        $sql = "SELECT id AS value, Nazwa AS label FROM " . $this->_getTableName('kowal_integracjaartpol_magazyny');
        return $connection->fetchAssoc(
            $sql
        );
    }

    private function getArray()
    {
        $arr = [];
        $magazyny = $this->getOptionArray();
        if (is_array($magazyny)) {
            foreach ($magazyny as $magazyn) {
                $arr[$magazyn['value']] = $magazyn['Nazwa'];
            }
        }
        return $arr;
    }


    /**
     * @return mixed
     */
    private function _getReadConnection()
    {
        return $this->_getConnection('core_read');
    }

    /**
     * @return mixed
     */
    private function _getWriteConnection()
    {
        return $this->_getConnection('core_write');
    }

    /**
     * @param string $type
     * @return mixed
     */
    private function _getConnection($type = 'core_read')
    {
        return $this->resourceConnection->getConnection($type);
    }

    /**
     * @param $tableName
     * @return mixed
     */
    private function _getTableName($tableName)
    {
        return $this->resourceConnection->getTableName($tableName);
    }
}
