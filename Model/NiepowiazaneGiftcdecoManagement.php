<?php
/**
 * A Magento 2 module named Kowal/IntegracjaArtpol
 * Copyright (C) 2019
 *
 * This file included in Kowal/IntegracjaArtpol is licensed under OSL 3.0
 *
 * http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * Please see LICENSE.txt for the full text of the OSL 3.0 license
 */

namespace Kowal\IntegracjaArtpol\Model;

use Kowal\IntegracjaArtpol\Api\NiepowiazaneGiftcdecoManagementInterface;
use Kowal\IntegracjaArtpol\lib\MagentoService;

class NiepowiazaneGiftcdecoManagement implements NiepowiazaneGiftcdecoManagementInterface
{
    /**
     * @var MagentoService
     */
    protected $magentoService;


    public function __construct(
        MagentoService $magentoService
    )
    {
        $this->magentoService = $magentoService;
    }

    /**
     * {@inheritdoc}
     */
    public function getNiepowiazaneGiftcdeco()
    {
        $niepowiazane__ = [];
        $niepowiazane = $this->magentoService->_getNiepowiazaneProduktyByAttr("GIFTDECO_");
        if (is_array($niepowiazane)) {
            foreach ($niepowiazane as $n) {
                $niepowiazane__[$n['id']] = $n['sku'];
            }
        }
        return $niepowiazane__;
    }
}