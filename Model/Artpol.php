<?php


namespace Kowal\IntegracjaArtpol\Model;

use Kowal\IntegracjaArtpol\Api\Data\ArtpolInterfaceFactory;
use Kowal\IntegracjaArtpol\Api\Data\ArtpolInterface;
use Kowal\IntegracjaArtpol\Model\ResourceModel\Artpol\Collection;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;

class Artpol extends AbstractModel
{

    protected $_eventPrefix = 'kowal_integracjaartpol_artpol';
    protected $dataObjectHelper;

    protected $artpolDataFactory;


    /**
     * @param Context $context
     * @param Registry $registry
     * @param ArtpolInterfaceFactory $artpolDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param ResourceModel\Artpol $resource
     * @param Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ArtpolInterfaceFactory $artpolDataFactory,
        DataObjectHelper $dataObjectHelper,
        ResourceModel\Artpol $resource,
        Collection $resourceCollection,
        array $data = []
    ) {
        $this->artpolDataFactory = $artpolDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve artpol model with artpol data
     * @return ArtpolInterface
     */
    public function getDataModel()
    {
        $artpolData = $this->getData();
        
        $artpolDataObject = $this->artpolDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $artpolDataObject,
            $artpolData,
            ArtpolInterface::class
        );
        
        return $artpolDataObject;
    }
}
