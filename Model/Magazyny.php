<?php


namespace Kowal\IntegracjaArtpol\Model;

use Kowal\IntegracjaArtpol\Api\Data\MagazynyInterfaceFactory;
use Kowal\IntegracjaArtpol\Api\Data\MagazynyInterface;
use Kowal\IntegracjaArtpol\Model\ResourceModel\Magazyny\Collection;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;

class Magazyny extends AbstractModel
{

    protected $magazynyDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'kowal_integracjaartpol_magazyny';

    /**
     * @param Context $context
     * @param Registry $registry
     * @param MagazynyInterfaceFactory $magazynyDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param ResourceModel\Magazyny $resource
     * @param Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        MagazynyInterfaceFactory $magazynyDataFactory,
        DataObjectHelper $dataObjectHelper,
        ResourceModel\Magazyny $resource,
        Collection $resourceCollection,
        array $data = []
    ) {
        $this->magazynyDataFactory = $magazynyDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve magazyny model with magazyny data
     * @return MagazynyInterface
     */
    public function getDataModel()
    {
        $magazynyData = $this->getData();
        
        $magazynyDataObject = $this->magazynyDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $magazynyDataObject,
            $magazynyData,
            MagazynyInterface::class
        );
        
        return $magazynyDataObject;
    }
}
