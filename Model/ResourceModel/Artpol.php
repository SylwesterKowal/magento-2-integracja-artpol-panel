<?php


namespace Kowal\IntegracjaArtpol\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Artpol extends AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('kowal_integracjaartpol_artpol', 'artpol_id');
    }
}
