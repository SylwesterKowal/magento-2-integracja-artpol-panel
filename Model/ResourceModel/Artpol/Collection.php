<?php


namespace Kowal\IntegracjaArtpol\Model\ResourceModel\Artpol;

use Kowal\IntegracjaArtpol\Model\ResourceModel\Artpol;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Kowal\IntegracjaArtpol\Model\Artpol::class,
            Artpol::class
        );
    }
}
