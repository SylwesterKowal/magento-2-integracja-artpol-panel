<?php


namespace Kowal\IntegracjaArtpol\Model\ResourceModel\Magazyny;

use Kowal\IntegracjaArtpol\Model\ResourceModel\Magazyny;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Kowal\IntegracjaArtpol\Model\Magazyny::class,
            Magazyny::class
        );
    }
}
