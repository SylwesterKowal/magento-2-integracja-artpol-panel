<?php


namespace Kowal\IntegracjaArtpol\Model;

use Exception;
use Kowal\IntegracjaArtpol\Api\Data\ArtpolInterface;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Kowal\IntegracjaArtpol\Api\ArtpolRepositoryInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Kowal\IntegracjaArtpol\Model\ResourceModel\Artpol\CollectionFactory as ArtpolCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\DataObjectHelper;
use Kowal\IntegracjaArtpol\Api\Data\ArtpolSearchResultsInterfaceFactory;
use Kowal\IntegracjaArtpol\Api\Data\ArtpolInterfaceFactory;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Exception\NoSuchEntityException;
use Kowal\IntegracjaArtpol\Model\ResourceModel\Artpol as ResourceArtpol;

class ArtpolRepository implements ArtpolRepositoryInterface
{

    protected $dataObjectProcessor;

    private $collectionProcessor;

    protected $extensibleDataObjectConverter;
    protected $dataObjectHelper;

    protected $extensionAttributesJoinProcessor;

    protected $artpolFactory;

    private $storeManager;

    protected $resource;

    protected $searchResultsFactory;

    protected $dataArtpolFactory;

    protected $artpolCollectionFactory;


    /**
     * @param ResourceArtpol $resource
     * @param ArtpolFactory $artpolFactory
     * @param ArtpolInterfaceFactory $dataArtpolFactory
     * @param ArtpolCollectionFactory $artpolCollectionFactory
     * @param ArtpolSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceArtpol $resource,
        ArtpolFactory $artpolFactory,
        ArtpolInterfaceFactory $dataArtpolFactory,
        ArtpolCollectionFactory $artpolCollectionFactory,
        ArtpolSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->artpolFactory = $artpolFactory;
        $this->artpolCollectionFactory = $artpolCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataArtpolFactory = $dataArtpolFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        ArtpolInterface $artpol
    ) {
        /* if (empty($artpol->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $artpol->setStoreId($storeId);
        } */
        
        $artpolData = $this->extensibleDataObjectConverter->toNestedArray(
            $artpol,
            [],
            ArtpolInterface::class
        );
        
        $artpolModel = $this->artpolFactory->create()->setData($artpolData);
        
        try {
            $this->resource->save($artpolModel);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the artpol: %1',
                $exception->getMessage()
            ));
        }
        return $artpolModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($artpolId)
    {
        $artpol = $this->artpolFactory->create();
        $this->resource->load($artpol, $artpolId);
        if (!$artpol->getId()) {
            throw new NoSuchEntityException(__('Artpol with id "%1" does not exist.', $artpolId));
        }
        return $artpol->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        SearchCriteriaInterface $criteria
    ) {
        $collection = $this->artpolCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            ArtpolInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        ArtpolInterface $artpol
    ) {
        try {
            $artpolModel = $this->artpolFactory->create();
            $this->resource->load($artpolModel, $artpol->getArtpolId());
            $this->resource->delete($artpolModel);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Artpol: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($artpolId)
    {
        return $this->delete($this->getById($artpolId));
    }
}
