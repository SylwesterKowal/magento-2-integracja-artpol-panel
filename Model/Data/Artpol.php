<?php


namespace Kowal\IntegracjaArtpol\Model\Data;

//use Kowal\IntegracjaArtpol\Api\Data\ArtpolExtensionInterface;
use Kowal\IntegracjaArtpol\Api\Data\ArtpolInterface;
use Magento\Framework\Api\AbstractExtensibleObject;

class Artpol extends AbstractExtensibleObject implements ArtpolInterface
{

    /**
     * Get artpol_id
     * @return string|null
     */
    public function getArtpolId()
    {
        return $this->_get(self::ARTPOL_ID);
    }

    /**
     * Set artpol_id
     * @param string $artpolId
     * @return ArtpolInterface
     */
    public function setArtpolId($artpolId)
    {
        return $this->setData(self::ARTPOL_ID, $artpolId);
    }

    /**
     * Get id
     * @return string|null
     */
    public function getId()
    {
        return $this->_get(self::ID);
    }

    /**
     * Set id
     * @param string $id
     * @return ArtpolInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

//    /**
//     * Retrieve existing extension attributes object or create a new one.
//     * @return ArtpolExtensionInterface|null
//     */
//    public function getExtensionAttributes()
//    {
//        return $this->_getExtensionAttributes();
//    }
//
//    /**
//     * Set an extension attributes object.
//     * @param ArtpolExtensionInterface $extensionAttributes
//     * @return $this
//     */
//    public function setExtensionAttributes(
//        ArtpolExtensionInterface $extensionAttributes
//    ) {
//        return $this->_setExtensionAttributes($extensionAttributes);
//    }

    /**
     * Get name
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set name
     * @param string $name
     * @return ArtpolInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Get price
     * @return string|null
     */
    public function getPrice()
    {
        return $this->_get(self::PRICE);
    }

    /**
     * Set price
     * @param string $price
     * @return ArtpolInterface
     */
    public function setPrice($price)
    {
        return $this->setData(self::PRICE, $price);
    }

    /**
     * Get symbol
     * @return string|null
     */
    public function getSymbol()
    {
        return $this->_get(self::SYMBOL);
    }

    /**
     * Set symbol
     * @param string $symbol
     * @return ArtpolInterface
     */
    public function setSymbol($symbol)
    {
        return $this->setData(self::SYMBOL, $symbol);
    }

    /**
     * Get cat
     * @return string|null
     */
    public function getCat()
    {
        return $this->_get(self::CAT);
    }

    /**
     * Set cat
     * @param string $cat
     * @return ArtpolInterface
     */
    public function setCat($cat)
    {
        return $this->setData(self::CAT, $cat);
    }

    /**
     * Get url
     * @return string|null
     */
    public function getUrl()
    {
        return $this->_get(self::URL);
    }

    /**
     * Set url
     * @param string $url
     * @return ArtpolInterface
     */
    public function setUrl($url)
    {
        return $this->setData(self::URL, $url);
    }

    /**
     * Get magazyny
     * @return string|null
     */
    public function getMagazyny()
    {
        return $this->_get(self::MAGAZYNY);
    }

    /**
     * Set magazyny
     * @param string $magazyny
     * @return ArtpolInterface
     */
    public function setMagazyny($magazyny)
    {
        return $this->setData(self::MAGAZYNY, $magazyny);
    }

    /**
     * Get weight
     * @return string|null
     */
    public function getWeight()
    {
        return $this->_get(self::WEIGHT);
    }

    /**
     * Set weight
     * @param string $weight
     * @return ArtpolInterface
     */
    public function setWeight($weight)
    {
        return $this->setData(self::WEIGHT, $weight);
    }

    /**
     * Get stocks
     * @return string|null
     */
    public function getStocks()
    {
        return $this->_get(self::STOCKS);
    }

    /**
     * Set stocks
     * @param string $stocks
     * @return ArtpolInterface
     */
    public function setStocks($stocks)
    {
        return $this->setData(self::STOCKS, $stocks);
    }

    /**
     * Get powiazany
     * @return string|null
     */
    public function getPowiazany()
    {
        return $this->_get(self::POWIAZANY);
    }

    /**
     * Set powiazany
     * @param string $powiazany
     * @return ArtpolInterface
     */
    public function setPowiazany($powiazany)
    {
        return $this->setData(self::POWIAZANY, $powiazany);
    }

    /**
     * Get last_update
     * @return string|null
     */
    public function getLastUpdate()
    {
        return $this->_get(self::LAST_UPDATE);
    }

    /**
     * Set last_update
     * @param string $lastUpdate
     * @return ArtpolInterface
     */
    public function setLastUpdate($lastUpdate)
    {
        return $this->setData(self::LAST_UPDATE, $lastUpdate);
    }
}
