<?php


namespace Kowal\IntegracjaArtpol\Model\Data;

//use Kowal\IntegracjaArtpol\Api\Data\MagazynyExtensionInterface;
use Kowal\IntegracjaArtpol\Api\Data\MagazynyInterface;
use Magento\Framework\Api\AbstractExtensibleObject;

class Magazyny extends AbstractExtensibleObject implements MagazynyInterface
{

    /**
     * Get magazyny_id
     * @return string|null
     */
    public function getMagazynyId()
    {
        return $this->_get(self::MAGAZYNY_ID);
    }

    /**
     * Set magazyny_id
     * @param string $magazynyId
     * @return MagazynyInterface
     */
    public function setMagazynyId($magazynyId)
    {
        return $this->setData(self::MAGAZYNY_ID, $magazynyId);
    }

    /**
     * Get id
     * @return string|null
     */
    public function getId()
    {
        return $this->_get(self::ID);
    }

    /**
     * Set id
     * @param string $id
     * @return MagazynyInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

//    /**
//     * Retrieve existing extension attributes object or create a new one.
//     * @return MagazynyExtensionInterface|null
//     */
//    public function getExtensionAttributes()
//    {
//        return $this->_getExtensionAttributes();
//    }
//
//    /**
//     * Set an extension attributes object.
//     * @param MagazynyExtensionInterface $extensionAttributes
//     * @return $this
//     */
//    public function setExtensionAttributes(
//        MagazynyExtensionInterface $extensionAttributes
//    ) {
//        return $this->_setExtensionAttributes($extensionAttributes);
//    }

    /**
     * Get Nazwa
     * @return string|null
     */
    public function getNazwa()
    {
        return $this->_get(self::NAZWA);
    }

    /**
     * Set Nazwa
     * @param string $nazwa
     * @return MagazynyInterface
     */
    public function setNazwa($nazwa)
    {
        return $this->setData(self::NAZWA, $nazwa);
    }
}
