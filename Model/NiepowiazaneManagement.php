<?php


namespace Kowal\IntegracjaArtpol\Model;

use Kowal\IntegracjaArtpol\Api\NiepowiazaneManagementInterface;
use Kowal\IntegracjaArtpol\lib\MagentoService;

class NiepowiazaneManagement implements NiepowiazaneManagementInterface
{

    /**
     * @var MagentoService
     */
    protected $magentoService;


    public function __construct(
        MagentoService $magentoService
    )
    {
        $this->magentoService = $magentoService;
    }

    /**
     * {@inheritdoc}
     */
    public function getNiepowiazane()
    {

        $niepowiazane__ = [];
        $niepowiazane = $this->magentoService->_getNiepowiazaneProdukty();
        if (is_array($niepowiazane)) {
            foreach ($niepowiazane as $n) {
                $niepowiazane__[$n['id']] = $n['id'];
            }
        }
        return $niepowiazane__;
    }

}