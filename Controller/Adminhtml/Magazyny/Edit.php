<?php


namespace Kowal\IntegracjaArtpol\Controller\Adminhtml\Magazyny;

use Kowal\IntegracjaArtpol\Model\Magazyny;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Edit extends \Kowal\IntegracjaArtpol\Controller\Adminhtml\Magazyny
{

    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('magazyny_id');
        $model = $this->_objectManager->create(Magazyny::class);
        
        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Magazyny no longer exists.'));
                /** @var Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('kowal_integracjaartpol_magazyny', $model);
        
        // 3. Build edit form
        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Magazyny') : __('New Magazyny'),
            $id ? __('Edit Magazyny') : __('New Magazyny')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Magazynys'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Edit Magazyny %1', $model->getId()) : __('New Magazyny'));
        return $resultPage;
    }
}
