<?php


namespace Kowal\IntegracjaArtpol\Controller\Adminhtml\Magazyny;

use Exception;
use Kowal\IntegracjaArtpol\Model\Magazyny;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;

class Delete extends \Kowal\IntegracjaArtpol\Controller\Adminhtml\Magazyny
{

    /**
     * Delete action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('magazyny_id');
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create(Magazyny::class);
                $model->load($id);
                $model->delete();
                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the Magazyny.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['magazyny_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Magazyny to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
