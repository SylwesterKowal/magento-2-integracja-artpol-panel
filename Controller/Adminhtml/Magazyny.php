<?php


namespace Kowal\IntegracjaArtpol\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Registry;

abstract class Magazyny extends Action
{

    protected $_coreRegistry;
    const ADMIN_RESOURCE = 'Kowal_IntegracjaArtpol::top_level';

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Init page
     *
     * @param Page $resultPage
     * @return Page
     */
    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE)
            ->addBreadcrumb(__('Kowal'), __('Kowal'))
            ->addBreadcrumb(__('Magazyny'), __('Magazyny'));
        return $resultPage;
    }
}
