<?php


namespace Kowal\IntegracjaArtpol\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\InstallSchemaInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $table_kowal_integracjaartpol_artpol = $setup->getConnection()->newTable($setup->getTable('kowal_integracjaartpol_artpol'));

        $table_kowal_integracjaartpol_artpol->addColumn(
            'artpol_id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,],
            'Entity ID'
        );

        $table_kowal_integracjaartpol_artpol->addColumn(
            'id',
            Table::TYPE_TEXT,
            255,
            ['nullable' => False],
            'id'
        );

        $table_kowal_integracjaartpol_artpol->addColumn(
            'name',
            Table::TYPE_TEXT,
            255,
            [],
            'name'
        );

        $table_kowal_integracjaartpol_artpol->addColumn(
            'price',
            Table::TYPE_DECIMAL,
            '12,4',
            ['unsigned' => true],
            'price'
        );

        $table_kowal_integracjaartpol_artpol->addColumn(
            'symbol',
            Table::TYPE_TEXT,
            255,
            [],
            'symbol'
        );

        $table_kowal_integracjaartpol_artpol->addColumn(
            'cat',
            Table::TYPE_TEXT,
            255,
            [],
            'cat'
        );

        $table_kowal_integracjaartpol_artpol->addColumn(
            'url',
            Table::TYPE_TEXT,
            255,
            [],
            'url'
        );

        $table_kowal_integracjaartpol_artpol->addColumn(
            'magazyny',
            Table::TYPE_TEXT,
            null,
            [],
            'magazyny'
        );

        $table_kowal_integracjaartpol_artpol->addColumn(
            'weight',
            Table::TYPE_DECIMAL,
            '12,4',
            [],
            'weight'
        );

        $table_kowal_integracjaartpol_artpol->addColumn(
            'stocks',
            Table::TYPE_NUMERIC,
            null,
            [],
            'stocks'
        );

        $table_kowal_integracjaartpol_artpol->addColumn(
            'powiazany',
            Table::TYPE_BOOLEAN,
            null,
            [],
            'powiazany'
        );

        $table_kowal_integracjaartpol_artpol->addColumn(
            'last_update',
            Table::TYPE_DATETIME,
            null,
            [],
            'last_update'
        );

        $table_kowal_integracjaartpol_magazyny = $setup->getConnection()->newTable($setup->getTable('kowal_integracjaartpol_magazyny'));

        $table_kowal_integracjaartpol_magazyny->addColumn(
            'magazyny_id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,],
            'Entity ID'
        );

        $table_kowal_integracjaartpol_magazyny->addColumn(
            'id',
            Table::TYPE_NUMERIC,
            null,
            [],
            'id'
        );

        $table_kowal_integracjaartpol_magazyny->addColumn(
            'Nazwa',
            Table::TYPE_TEXT,
            255,
            [],
            'Nazwa'
        );

        $setup->getConnection()->createTable($table_kowal_integracjaartpol_magazyny);

        $setup->getConnection()->createTable($table_kowal_integracjaartpol_artpol);
    }
}
